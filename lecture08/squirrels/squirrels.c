/* squirrel.c: Squirrel Hunt */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Constants */

#define NMAX_SQUARES 100

/* Global Variables */

int G[NMAX_SQUARES + 1][NMAX_SQUARES + 1];  // Grid
int S[NMAX_SQUARES + 1][NMAX_SQUARES + 1];  // Squirrel Table

/* Functions */

void read_grid(int n) {
    memset(G, 0, sizeof(int) * (NMAX_SQUARES + 1) * (NMAX_SQUARES + 1));

    for (int r = 1; r <= n; r++) {
	for (int c = 1; c <= n; c++) {
	    scanf("%d", &(G[r][c]));
	}
    }
}

int max(const int a, const int b) {
    return ((a > b) ? a : b);
}

void compute_table(int n) {
    memset(S, 0, sizeof(int) * (NMAX_SQUARES + 1) * (NMAX_SQUARES + 1));

    for (int r = 1; r <= n; r++) {
	for (int c = 1; c <= n; c++) {
	    S[r][c] = max(S[r][c - 1], S[r - 1][c]) + G[r][c];
	}
    }
}

/* Main Execution */

int main(int argc, char* argv[]) {
    int n;

    while (scanf("%d", &n) != EOF) {
	read_grid(n);
	compute_table(n);
	printf("%d\n", S[n][n]);
    }

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 ft=c: */
