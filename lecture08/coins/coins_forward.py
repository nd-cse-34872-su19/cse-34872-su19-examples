#!/usr/bin/env python3

import sys

COINS = (1, 3, 4)

# Compute Coins Table

def compute_table(n, coins=COINS):
    # Initialize table to n
    table = [n] * (n + 1)

    # Initialize base cases (ie. coins)
    for coin in coins:
        table[coin] = 1

    # For each entry i in table, generate successive values:
    #
    #   table[i + coin] = min(table[i] + 1, table[i + coin])
    for i in range(1, n - max(coins) + 1):
        for coin in coins:
            table[i + coin] = min(table[i] + 1, table[i + coin])

    return table

# Main execution

if __name__ == '__main__':
    TABLE = compute_table(100)                  # Compute table

    for n in map(int, sys.stdin):
        print('{} = {}'.format(n, TABLE[n]))    # Lookup solutions in table
