#!/usr/bin/env python3

import sys

# Compute Cycle length

CYCLE_LENGTHS = {}

def cycle_length(n):
    if n == 1:
        return 1 

    if n not in CYCLE_LENGTHS:
        if n % 2 == 1:
            length = cycle_length(3*n + 1) + 1
        else:
            length = cycle_length(n / 2) + 1
    
        CYCLE_LENGTHS[n] = length

    return CYCLE_LENGTHS[n]

# Main execution

if __name__ == '__main__':
    for line in sys.stdin:
        start, end  = map(int, line.split())
        first, last = sorted((start, end))
        max_cycle   = 0
        max_integer = None

        for i in range(first, last + 1):
            cycle = cycle_length(i)
            if cycle > max_cycle:
                max_cycle   = cycle
                max_integer = i

        print(start, end, max_integer, max_cycle)
