#!/usr/bin/env python3

import itertools
import sys

# Functions

def sumitup_r(numbers, target, results, index, subset):
    # Base case: subset sums up to target, so store it
    if sum(subset) == target:
        results.add(tuple(subset))
        return

    # Base case: prune search when index is length of numbers, or sum of susbet
    # is greater than target
    if index == len(numbers) or sum(subset) > target:
        return

    # Recursive step: try with current number and without
    subset.append(numbers[index])   # Attempt with number
    sumitup_r(numbers, target, results, index + 1, subset)
    subset.pop(-1)                  # Attempt without number
    sumitup_r(numbers, target, results, index + 1, subset)

def sumitup(numbers, target):
    results = set()
    sumitup_r(numbers, target, results, 0, [])
    return sorted(results, reverse=True)

# Main execution

if __name__ == '__main__':
    for line in sys.stdin:
        data    = list(map(int, line.split()))
        target  = data[0]
        n       = data[1]
        numbers = data[2:]

        if target == 0:
            break

        print('Sums of {}:'.format(target))
        results = sumitup(numbers, target)
        if results:
            for result in results:
                print('+'.join(map(str, result)))
        else:
            print('NONE')
