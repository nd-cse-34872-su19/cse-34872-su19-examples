#!/usr/bin/env python3

import itertools
import sys

# Constants

LOTTO_NUMBERS = 6

# Functions

def display_combinations(s, n=LOTTO_NUMBERS):
    for combination in sorted(itertools.combinations(s, n)):
        print(' '.join(map(str, combination)))

# Main execution

if __name__ == '__main__':
    for line, numbers in enumerate(map(str.split, sys.stdin)):
        if len(numbers) <= 1:
            break
        
        if line:
            print('')

        display_combinations(map(int, numbers[1:]))
