#!/usr/bin/env python3

import sys

# Functions

def feed_children(children, cookies):
    count = 0

    while cookies and children:
        child  = children.pop(0)
        cookie = cookies[0]

        if child <= cookie:
            cookies.pop(0)
            count += 1

    return count

# Main execution

if __name__ == '__main__':
    children = sys.stdin.readline().split()
    cookies  = sys.stdin.readline().split()
    while children and cookies:
        children = sorted(map(int, children), reverse=True)
        cookies  = sorted(map(int, cookies) , reverse=True)

        print(feed_children(children, cookies))

        children = sys.stdin.readline().split()
        cookies  = sys.stdin.readline().split()
