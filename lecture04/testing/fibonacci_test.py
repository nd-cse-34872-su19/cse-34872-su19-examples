#!/usr/bin/env python3

import unittest
import fibonacci

# Test Cases

class TestFibonacci(unittest.TestCase):

    def test_fibonacci(self):
        self.assertEqual(fibonacci.fibonacci(1), 1)
        self.assertEqual(fibonacci.fibonacci(2), 1)
        self.assertEqual(fibonacci.fibonacci(3), 2)
        self.assertEqual(fibonacci.fibonacci(4), 3)
        self.assertEqual(fibonacci.fibonacci(5), 5)
        self.assertEqual(fibonacci.fibonacci(6), 8)

# Main Execution

if __name__ == '__main__':
    unittest.main()
