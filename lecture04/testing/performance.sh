#!/bin/bash

test-input() {
    cat <<EOF
1
2
3
4
5
40
EOF
}

test-output() {
    cat <<EOF
fibonacci(1) = 1
fibonacci(2) = 1
fibonacci(3) = 2
fibonacci(4) = 3
fibonacci(5) = 5
fibonacci(40) = 102334155
EOF
}

if [ $# -lt 1 ]; then
    echo "Usage: $0 PROGRAM"
    exit 1
fi

PROGRAM=$1
LOGFILE=performance.log

#echo -n "Testing $PROGRAM ... "
#diff -y <(test-input | timeout 5 $PROGRAM) <(test-output) &> $LOGFILE
echo -n "Timing $PROGRAM ... "
test-input | timeout 5 $PROGRAM &> $LOGFILE
if [ $? -eq 0 ]; then
    echo "Success"
else
    echo "Failure"
    echo
    cat $LOGFILE
fi

rm -f $LOGFILE
