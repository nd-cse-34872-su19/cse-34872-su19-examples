#!/usr/bin/env python3

import sys

# Globals

Fibonacci = {}

# Functions

def fibonacci(n):
    ''' Compute the nth number in the Fibonacci sequence.
    >>> fibonacci(1)
    1

    >>> fibonacci(5)
    5
    '''
    if n == 0:              # Base case
        return 0
    elif n <= 2:            # Base case
        return 1

    '''
                            # Recursive step
    return fibonacci(n - 1) + fibonacci(n - 2) 
    '''

    if n not in Fibonacci:  # Memoize recursive step
        Fibonacci[n] = fibonacci(n - 1) + fibonacci(n - 2) 

    return Fibonacci[n]

# Main execution

if __name__ == '__main__':
    for i in map(int, sys.stdin):
        print('fibonacci({}) = {}'.format(i, fibonacci(i)))
